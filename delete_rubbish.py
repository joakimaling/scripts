#!/usr/bin/python3
"""Deletes files in the rubbish bin removed prior to a given time"""
#                     ___           __  ___
#  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
# / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
# \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
#         https://gitlab.com/carolinealing    /___/
#
# ~/Scripts/delete_rubbish.py
#

# Common modules
from configparser import ConfigParser
from datetime import datetime
from os.path import expanduser, isdir, join, splitext
from shutil import rmtree
import os

# Utility modules
from tools.log import Log

# Path to the directory containing the rubbish
location: str = expanduser('~/.local/share/Trash')
parser: ConfigParser = ConfigParser(interpolation=None)

# Some useful constants
NOW: float = datetime.now().timestamp()
THRESHOLD: int = 7776000 # 90 days

# Setting up logs
log = Log(__file__)


def threshold_time(threshold: int = THRESHOLD) -> str:
	"""Converts a threshold number into a time string

	Converts and returns a threshold in seconds to a date-and-time string that
	number of seconds from now.

	Parameters
	----------
	threshold: int
		number in seconds in the past

	Returns
	-------
	str
		time-and-date string
	"""
	return datetime.strftime(
		datetime.fromtimestamp(NOW - threshold),
		'%Y-%m-%d %H:%M:%S'
	)


def is_old(timestamp: str, threshold: int = THRESHOLD) -> bool:
	"""Checks if a timestamp is before a threshold

	Returns true if the given time falls before a threshold amount of seconds
	in the past.

	Parameters
	----------
	timestamp: str
		timestamp to compare
	threshold: int
		number in seconds in the past

	Returns
	-------
	bool
		whether the timestamp is before the threshold
	"""
	time: datetime = datetime.strptime(timestamp, '%Y-%m-%dT%H:%M:%S')
	return time.timestamp() < NOW - threshold


log.info('Removing files older than %s', threshold_time())

for file in os.listdir(join(location, 'info')):
	try:
		file_name: str = splitext(file)[0]
		data_file: str = join(location, 'info', file)

		# Get information about the deleted file
		parser.read(join(location, 'info', file))
		_, deletion_date = parser.items('Trash Info')

		# Remove the file if it was deleted before a given time (see
		# is_old() function)
		if is_old(deletion_date[1]):
			actual_file: str = join(location, 'files', file_name)

			# If it's a directory, remove all the files & directories contained
			# within
			if isdir(actual_file):
				rmtree(actual_file)

			# Remove the actual file
			else:
				os.remove(actual_file)

			# Remove its data file
			os.remove(data_file)

			log.info("Removed '%s' from rubbish", file_name)

	# Log whether a file can't be read somehow & thus not removed
	except OSError as error:
		log.error("Can't remove '%s': %s", file_name, error)
		continue
