"""Creates stylish log files"""
#                     ___           __  ___
#  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
# / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
# \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
#         https://gitlab.com/carolinealing    /___/
#
# ~/Scripts/tools/log.py
#

# Common modules
from logging import Formatter, StreamHandler, getLoggerClass
from logging.handlers import RotatingFileHandler
from os.path import basename, splitext
import logging
import sys

# Log output formats
_CONSOLE_FORMAT: str = '%(levelname)-17s | %(filename)s:%(lineno)d | %(message)s'
_FILE_FORMAT: str = '%(asctime)s | %(levelname)8s | %(filename)s:%(lineno)d | %(message)s'

# Map colours to log levels
_COLOURS: dict[str, int] = {
	'CRITICAL': 41,
	'DEBUG': 36,
	'ERROR': 31,
	'INFO': 32,
	'WARNING': 33
}


class ColourFormatter(Formatter):
	"""Colourises console logs"""

	def __init__(self, log_format: str):
		"""Creates a colour formatter

		Creates a formatter used to colourise the log record for better
		distinguishment when logging to thr console.

		Parameters
		----------
		log_format: str
			format with which a record is output
		"""
		super().__init__(log_format)
		self.colourise = sys.stderr.isatty()

	def format(self, record):
		"""Adds colour to the log level

		Parameters
		----------
		record: LogRecord
			record to colourise, if printing to a TTY
		"""
		if self.colourise and record.levelname in _COLOURS:
			record.levelname = f'\033[{_COLOURS[record.levelname]}m{record.levelname}\033[0m'

		return super().format(record)


class Log(getLoggerClass()): # type: ignore
	"""Logs nicely formatted log to file & console"""
	log_file_path: str = ''

	def __init__(self, name: str, handlers: tuple[str, ...]|None = None):
		"""Create the logger & its handlers

		Parameters
		----------
		name: str
			name of the logger & log file
		handlers: tuple[str, ...]|None
			handlers to create, if empty a file handler is created
		"""
		name = splitext(basename(name))[0]
		super().__init__(name)

		# Output to console
		if handlers and 'console' in handlers:
			self.addHandler(self.get_console_handler())

		# Output to file (default)
		if not handlers or 'file' in handlers:
			self.addHandler(self.get_file_handler(name))


	def get_console_handler(self) -> StreamHandler:
		"""Creates a handler for output to the console

		Returns
		-------
		StreamHandler
			the handler
		"""
		formatter = ColourFormatter(_CONSOLE_FORMAT)

		handler = StreamHandler()
		handler.setFormatter(formatter)
		handler.setLevel(logging.DEBUG)

		return handler


	def get_file_handler(self, name: str) -> RotatingFileHandler:
		"""Creates a handler for output to a file

		Parameters
		----------
		name: str
			name of the file

		Returns
		-------
		RotatingFileHandler
			the handler
		"""
		formatter = Formatter(_FILE_FORMAT)

		self.log_file_path = f'/var/log/scripts/{name}.log'

		handler = RotatingFileHandler(
			self.log_file_path,
			backupCount=10,
			encoding='utf-8',
			maxBytes=1048576
		)

		handler.setFormatter(formatter)
		handler.setLevel(logging.INFO)

		return handler


	def get_path(self) -> str:
		"""Returns the log path.

		Returns
		-------
		str
			The log path.
		"""
		return self.log_file_path
