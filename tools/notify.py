"""Displays a notification"""
#                     ___           __  ___
#  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
# / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
# \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
#         https://gitlab.com/carolinealing    /___/
#
# ~/Scripts/tools/notify.py
#

# Utility modules
from . import process


def show(content: str, title: str = '', level: str = 'normal') -> int:
	"""Shows a notification

	Displays a notification with given content for the current desktop
	environment.

	Parameters
	----------
	content: str
		notification's content
	title: str
		(optional) notification's title
	level: str
		(optional) severity level [low, normal, critical]

	Returns
	-------
	int
		exit code
	"""
	return process.run_background((
		'/usr/bin/notify-send', '-u', level, title or 'Script Error', content
	))
