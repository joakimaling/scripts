"""Reads configuration files into Python"""
#                     ___           __  ___
#  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
# / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
# \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
#         https://gitlab.com/carolinealing    /___/
#
# ~/Scripts/tools/log.py
#

# Common modules
from configparser import ConfigParser
import json


class DottedDictionary(dict):
	"""Dotted dictionary"""
	__delattr__ = dict.__delitem__
	__getattr__ = dict.__getitem__
	__setattr__ = dict.__setitem__


def read(filename: str = '.env', form: str = 'ini') -> DottedDictionary:
	"""Reads a configuration file

	Returns the content of provided configuration file as a Python type.

	Parameters
	----------
	file: str
		Path to the file to read

	Returns
	-------
	DottedDictionary
		Content of the file
	"""
	data: dict = {}

	if form == 'ini':
		parser = ConfigParser()
		parser.read(filename)

		for section in parser.sections():
			data[section] = {}

			for key, value in parser.items(section):
				data[section][key] = value

	elif form == 'json':
		with open(filename, 'r', encoding='utf-8') as file:
			data = json.load(file)

	else:
		raise ValueError(f'File type {form} not supported')

	return DottedDictionary(data)
