"""Runs a command & returns formatted data"""
#                     ___           __  ___
#  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
# / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
# \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
#         https://gitlab.com/carolinealing    /___/
#
# ~/Scripts/tools/process.py
#

# Common modules
from subprocess import CompletedProcess, run as run_process


def run(command: str|tuple[str, ...], data: str = '') -> tuple[int, str, str]:
	"""Runs a command

	Runs the given command, optionally taking data to be piped in & returns its
	output nicely formatted.

	Parameters
	----------
	command: str|tuple[str, ...]
		command to run, may be a string or a tuple or string if more complex
	data: str
		input data, fed to `stdin` of the command

	Returns
	-------
	tuple[int, str, str]
		exit code, output & error nicely formatted
	"""
	result: CompletedProcess = run_process(
		command if isinstance(command, tuple) else command.split(' '),
		capture_output=True,
		check=False,
		input=data.encode('utf-8') if data else None
	)

	return (
		result.returncode,
		result.stderr.decode('utf-8'),
		result.stdout.decode('utf-8')
	)


def run_background(command: str|tuple[str, ...], data: str = '') -> int:
	"""Runs a command in the background

	Runs the given command in the background, optionally taking data to be
	piped in.

	Parameters
	----------
	command: str|tuple[str, ...]
		command to run, may be a string or a tuple or string if more complex
	data: str
		input data, fed to `stdin` of the command

	Returns
	-------
	int
		return code
	"""
	result: CompletedProcess = run_process(
		command if isinstance(command, tuple) else command.split(' '),
		capture_output=False,
		check=False,
		input=data.encode('utf-8') if data else None
	)

	return result.returncode
