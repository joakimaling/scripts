"""Sends e-mails using a default e-mail account"""
#                     ___           __  ___
#  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
# / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
# \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
#         https://gitlab.com/carolinealing    /___/
#
# ~/Scripts/tools/mail.py
#

# Common modules
from email.header import Header
from email.mime.text import MIMEText
from email.utils import formatdate
from smtplib import SMTP, SMTPException
import os

# Third-party modules
from dotenv import load_dotenv

# Import secrets
load_dotenv()

# Mail server settings
mail_host: str = os.getenv('MAIL_HOST') # type: ignore
mail_password: str = os.getenv('MAIL_PASSWORD') # type: ignore
mail_port: int = int(os.getenv('MAIL_PORT')) # type: ignore
mail_user: str = os.getenv('MAIL_USER') # type: ignore

# Default sender & recipient
mail_from: str = mail_user
mail_to: str = os.getenv('MAIL_ADDRESS') # type: ignore


def send(message: str, subject: str = '') -> int:
	"""Sends an e-mail

	Sends provided message as a plain text e-mail using a default mail server.
	An optional subejct may also be provided. Used to send notifications to a
	user telling about the outcome of scripts.

	Parameters
	----------
	message: str
		message to be sent
	subject: str
		(optional) subject of the message

	Returns
	-------
	int
		exit code
	"""
	content = MIMEText(message, 'plain', 'utf-8')
	content['Date'] = formatdate(localtime=True)
	content['From'] = mail_from
	content['Subject'] = Header(subject or 'Script Error', 'utf-8')
	content['To'] = mail_to

	try:
		with SMTP(mail_host, port=mail_port, timeout=10) as mailer:
			mailer.starttls()
			mailer.login(mail_user, mail_password)
			mailer.sendmail(mail_from, [mail_to], content.as_string())

	except SMTPException:
		return 1

	return 0
