#!/usr/bin/python3
"""Creates a custom Argument Parser"""
#                     ___           __  ___
#  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
# / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
# \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
#         https://gitlab.com/carolinealing    /___/
#
# ~/Scripts/tools/parser.py
#

# Common modules
from argparse import ArgumentDefaultsHelpFormatter, ArgumentParser
from datetime import date
from operator import attrgetter


class CustomHelpFormatter(ArgumentDefaultsHelpFormatter):
	"""
	Extends the formatter to also sort the arguments alphabetically	when
	printing the help text.
	"""

	def add_arguments(self, actions):
		actions = sorted(actions, key=attrgetter('option_strings'))
		super().add_arguments(actions)


class CustomArgumentParser(ArgumentParser):
	"""
	Extends the argument parser to sort the list of arguments alphabetically,
	add information about default values to the help texts, add a default
	epilogue & prefix the description.
	"""

	def __init__(self, **data):
		description: str = data.pop('description')
		super().__init__(
			description=f'Penguin Lair {description}',
			epilog=f'(c) {date.today().year} Caroline Åling',
			formatter_class=lambda prog: CustomHelpFormatter(
				prog,
				max_help_position=50,
				width=100
			),
			**data
		)
