#!/usr/bin/python3
"""Turns the WiFi on/off depending on the connection"""
#                     ___           __  ___
#  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
# / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
# \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
#         https://gitlab.com/carolinealing    /___/
#
# ~/Scripts/toggle_network.py
#

# Common modules
from time import sleep
import os
import sys

# Third-party modules
from dotenv import load_dotenv

# Utility modules
from tools import process
from tools.log import Log

# Import secrets
load_dotenv()

# Names of the interfaces used
ETHERNET: str = os.getenv('ETHERNET_INTERFACE') # type: ignore
WIRELESS: str = os.getenv('WIRELESS_INTERFACE') # type: ignore

# Profile name for VPN connection
VPN_NAME: str = os.getenv('VPN_NAME') # type: ignore

# Address to the office network's gateway router
GATEWAY: str = os.getenv('GATEWAY') # type: ignore

# Get the action & interface from NetworkManager
ACTION: str = sys.argv[2]
INTERFACE: str = sys.argv[1]

# Setting up logs
log = Log(__file__)


def check_address(address: str, interface: str) -> bool:
	"""Returns true if address of interface matches given.

	Parameters
	----------
	address: str
		address to match against
	interface: str
		interface whose address to check

	Returns
	-------
	bool
		whether there's a match
	"""
	code, _, output = process.run(f'nmcli -g IP4 device show {interface}')

	# Upon failure return false
	if code != 0:
		return False

	# Check if address matches office gateway's
	return address in output


if INTERFACE == ETHERNET:
	exit_code: int = 0

	# Enable WiFi when Ethernet is disconnected
	if ACTION == 'down':
		log.info("'%s' disconnected. Enabling '%s'", ETHERNET, WIRELESS)

		# Turn on WiFi
		exit_code = process.run_background('nmcli radio wifi on')

		# Turn on VPN
		if exit_code == 0:
			sleep(1)
			log.info("Turning on VPN profile '%s'", VPN_NAME)
			exit_code = process.run_background(f'nmcli connect up id {VPN_NAME}')

	# Disable WiFi when Ethernet is connected
	elif ACTION == 'up':
		log.info("'%s' connected. Disabling '%s'", ETHERNET, WIRELESS)

		# Turn off VPN (by first checking whether we're at the office)
		if check_address(GATEWAY, INTERFACE):
			log.info("Turning off VPN profile '%s'", VPN_NAME)
			exit_code = process.run_background(f'nmcli connect down id {VPN_NAME}')

		# Turn off WiFi
		if exit_code == 0:
			exit_code = process.run_background('nmcli radio wifi off')

	# Log an error message upon failure
	if exit_code != 0:
		log.error('Unable to toggle network connection')

	sys.exit(exit_code)
