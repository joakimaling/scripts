#!/usr/bin/python3
#                     ___           __  ___
#  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
# / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
# \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
#         https://gitlab.com/carolinealing    /___/
#
# ~/Scripts/create_network_code.py
#
# https://github.com/zxing/zxing/wiki/Barcode-Contents#wi-fi-network-config-android-ios-11
#

# Common modules
from argparse import Namespace
from dataclasses import dataclass
import sys

# Utility modules
from tools.parser import CustomArgumentParser
from generate_code import CodeGenerator


@dataclass
class NetworkCodeGenerator:
	"""Creates a WiFi QR code

	Creates a QR code with WiFi information and outputs it to the given path as
	an image file. If target isn't provided it'll be ouput to the console.

	Parameters
	----------
	name: str
		Network name (SSID)
	hidden: bool
		Whether the network is hidden or not
	password: str
		Password for authentication
	security: str
		Encryption method
	target: str | None
		Path to where the resulting image will be stored. If not provided the
		QR code will be printend to the console
	"""
	name: str
	hidden: bool = False
	password: str = ''
	security: str = 'nopass'
	target: str | None = None

	@staticmethod
	def _escape(glyphs: str) -> str:
		symbols: tuple[str, ...] = ('\\', ':', ';', ',', '"')
		return ''.join([f'\\{glyph}' if glyph in symbols else glyph for glyph in glyphs])

	def run(self) -> int:
		"""Generates a QR code for WiFI connectivity.

		Returns
		-------
		int
			Return code
		"""
		hidden: str = 'H:true;' if self.hidden else ''
		name: str = self._escape(self.name)
		password: str = self._escape(self.password)
		security: str = self.security

		# Construct the MeCard-like string
		data: str = f'WIFI:{hidden}P:{password};S:{name};T:{security};;'

		# Create & print the QR code
		CodeGenerator(data, self.target)

		return 0


def main() -> int:
	"""Creates a QR code for WiFi connectivity.

	Returns
	-------
	int
		Return code
	"""
	parser: CustomArgumentParser = CustomArgumentParser(
		description=''
	)

	parser.add_argument(
		'-p', '--password',
		help='Password for authentication'
	)

	parser.add_argument(
		'--security',
		choices=('WPA', 'nopass'),
		default='nopass',
		help='Encryption method'
	)

	parser.add_argument(
		'--ssid',
		dest='name',
		help='Network name (SSID)',
		required=True
	)

	parser.add_argument(
		'--hidden',
		action='store_true',
		help='Whether the network is hidden or not'
	)

	parser.add_argument(
		'-t', '--target',
		help='Path to where the resulting image will be stored'
	)

	# Run the parser and return the result
	data: Namespace = parser.parse_args()

	# Require password only if security is set
	if data.security != 'nopass' and data.password is None:
		parser.error('password is required if security is set')

	# Run the code generator
	return NetworkCodeGenerator(**data.__dict__).run()


if __name__ == '__main__':
	sys.exit(main())
