#!/usr/bin/python
"""Fetch petrol prices & store in a database"""
#                     ___           __  ___
#  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
# / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
# \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
#         https://gitlab.com/carolinealing    /___/
#
# ~/Scripts/paste_secret.py
#

from argparse import ArgumentParser, Namespace
from urllib.request import urlopen
import json
import matplotlib.pyplot as plot
import sys

PriceList = dict[str, float]


def fetch_price_list() -> str:
    # 'https://www.ingo.se/export/price/4'
    return '~/Downloads/historical-prices.csv'


def parse_price_list(path: str) -> PriceList:
    with open(path, 'r', encoding='utf-8') as file:
        for row in file.read().split('\n'):
            _, price, date = row.split('",')
            print(price.ltrim('"'))


def store_price_list(price_list: PriceList) -> int:
    return 0


def write_price_list(price_list: PriceList) -> None:

    pass


def main():
    file_path: str = fetch_price_list()
    parse_price_list(file_path)

if __name__ == '__main__':
    sys.exit(main())


"""
def get_prices():
    now = datetime.utcnow()
    starttime = now.strftime("%Y-%m-%dT%H:%M")
    endtime = now + timedelta(hours=30)
    endtimereal = endtime.strftime("%Y-%m-%dT%H:%M")
    ret = {}

    try:
        with urllib.request.urlopen("https://api.energidataservice.dk/dataset/elspotprices?start={...".format(starttime = starttime, endtime = endtimereal)) as url:
            data = json.load(url)
            for record in data['records']:
                ret.update({record['HourUTC'].replace('T', ' '): record['SpotPriceEUR']})

            return ret
    except Exception as e:
        print(e)
        raise SystemExit(1)

if __name__ == '__main__':
    dbconnect = MySQLdb.connect("host", "user", "pass", "db")
    cursor = dbconnect.cursor()
    prices = get_prices()
    for key, value in prices.items():
        #print('{} {}'.format(key, value))
        try:
            cursor.execute("REPLACE INTO se3 (timestamp, price) VALUES ('{}', '{}')".format(key, value))
            dbconnect.commit()
        except Exception as e:
            print('Error:{}'.format(e))
            print('{} {}'.format(key, value))
            pass
    dbconnect.close()
"""
