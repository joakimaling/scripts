#!/usr/bin/python3
"""Lists available updates & their versions"""
#                     ___           __  ___
#  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
# / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
# \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
#         https://gitlab.com/carolinealing    /___/
#
# ~/Scripts/list_updates.py
#
# Requires: sudo pacman -Sy
#

# Common modules
import sys

# Utility modules
from tools import mail, process
from tools.log import Log

# Variables
code: int
content: str = ''
current: str
current_head: str = 'Current version'
current_size: int = len(current_head)
data: str
latest: str
latest_head: str = 'Latest version'
latest_size: int = len(latest_head)
name_head: str = 'Package name'
name_size: int = len(name_head)
name: str
packages: list[tuple[str, ...]] = []

# Setting up logs
log = Log(__file__)

# Get pending updates
code, _, data = process.run('pacman -Qu')

# Exit if command failed
if code > 0:
	log.error('Run `pacman -Sy` first' if code == 1 else 'Nothing to do')

# Get the important data to populate the list with
else:
	for package in data.split('\n'):
		if len(package):
			name, current, _, latest = package.split(' ')
			packages.append((name, current, latest))

			current_size = max(current_size, len(current))
			latest_size = max(latest_size, len(latest))
			name_size = max(name_size, len(name))

	# Build the list
	if len(packages) > 0:
		for name, current, latest in packages:
			content += f'|{name:<{name_size}}|{current:>{current_size}}|{latest:>{latest_size}}|\n'

		line: str = '+' + '+'.join(('-' * name_size, '-' * current_size, '-' * latest_size)) + '+\n'
		note: str = f'{line}|{name_head:^{name_size}}|{current_head:^{current_size}}|'
		note += f'{latest_head:^{latest_size}}|\n{line}{content}{line}'

		# Send an e-mail with the list
		code = mail.send(note, 'Packages') # pylint: disable=C0103

		# Log the result
		if code > 0:
			log.error("Couldn't send message. Return code: %d", code)
		else:
			log.info('List sent successfully')

# Return the exit code
sys.exit(code)
