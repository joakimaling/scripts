#!/usr/bin/python3
"""Creates class file(s)"""
#                     ___           __  ___
#  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
# / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
# \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
#         https://gitlab.com/carolinealing    /___/
#
# ~/Scripts/create_class.py
#

# Common modules
from argparse import Namespace
from configparser import ConfigParser
from dataclasses import dataclass
from datetime import date
from os import makedirs
from os.path import expanduser, join
from textwrap import dedent
import sys

# Third-party modules
from jinja2 import Template

# Utility modules
from tools.parser import CustomArgumentParser

# Entry point file templates for some languages
entry_points: dict[str, str] = {
	'C': """\
		int main(int argc, char **argv) {
			return 0;
		}
		""",
	'C++': """\
		int main(int argc, char **argv) {
			return 0;
		}
		""",
	'JavaScript': """\
		'use strict';
		
		""",
	'Pascal': """\
		program {{ name | capitalize }};

		begin
			// ParamCount;
			// ParamStr(0);
		end.
		""",
	'PHP': """\
		
		"""
}

# Head to put at the top of each file
head: str = """\
{% if language == 'PHP' -%}
<?php

{% endif -%}
{{ comment_begin }}                    ___           __  ___
{{ comment }}  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
{{ comment }} / __/ _ `/ __/ _ \\/ / / _ \\/ -_) _ `/ / / _ \\/ _ `/
{{ comment }} \\__/\\_,_/_/  \\___/_/_/_//_/\\__/\\_,_/_/_/_//_/\\_, /
{{ comment }}         https://gitlab.com/carolinealing    /___/
{{ comment }}
{{ comment }} {{ path }}{{ name }}.{{ extension }}
{{ comment }} 
{{ comment }} (c) {{ year }} {{ author }}{% if not licence %}, All rights reserved.{% else %}
{{ comment }} This code is released under {{ licence }} licence. See LICENSE.md for more.{% endif %}
{{ comment_end }}
{{ '' }}
"""

# Header file templates for C/C++
headers: dict[str, str] = {
	'C': """\
		#ifndef {{ name | upper }}_H
		#define {{ name | upper }}_H

		void {{ name | lower }}(void);

		#endif
		""",
	'C++': """\
		#pragma once
		{% if base_class %}
		#include <{{ base_path }}{{ base_class | lower }}.hpp>
		{% endif -%}
		{% if namespace %}
		namespace {{ namespace | replace(' ', '::')  | lower }} {
		{%- endif %}
		{% set base = ': public ' + base_class | capitalize if base_class else '' -%}
		{% if namespace %}	{% endif %}class {{ name | capitalize }}{{ base }} {
		{% if namespace %}	{% endif %}	private:
		{% if namespace %}	{% endif %}	public:
		{% if namespace %}	{% endif %}		{{ name | capitalize }}();
		{% if namespace %}	{% endif %}		~{{ name | capitalize }}();
		{% if namespace %}	{% endif %}};{% if namespace %}
		}{% endif %}
		"""
}

# Source file templates for each language
sources: dict[str, str] = {
	'C': """\
		#include <{{ path }}{{ name | lower }}.h>

		void {{ name | lower }}(void) {

		}
		""",
	'C++': """\
		#include <{{ path }}{{ name | lower }}.hpp>
		{% if namespace %}
		namespace {{ namespace | replace(' ', '::')  | lower }} {
		{%- endif %}
		{% set base = ': ' + base_class | capitalize + '()' if base_class else '' -%}
		{% if namespace %}	{% endif %}{{ name | capitalize }}::{{ name | capitalize }}(){{ base }} {

		{% if namespace %}	{% endif %}}

		{% if namespace %}	{% endif %}{{ name | capitalize }}::~{{ name | capitalize }}() {

		{% if namespace %}	{% endif %}}{% if namespace %}
		}{% endif %}
		""",
	'Java': """\
		{%- if namespace %}
		package {{ namespace | replace(' ', '.') | capitalize }};
		{% endif %}
		{%- if base_class %}
		import {{ base_path | replace('/', '.') | lower }}{{ base_class | capitalize }};
		{% endif %}
		class {{ name | capitalize }}{% if base_class %} extends {{ base_class | capitalize }}{% endif %} {
			public {{ name | capitalize }}() {
				{%+ if base_class %}super();{% endif %}
			}
			{% if entry_point %}
			public static void main(String[] arguments) {

			}
			{% endif -%}
		}
		""",
	'JavaScript': """\
		{%- if base_class %}
		import {{ base_class | capitalize }} from '{{ base_path }}{{ base_class | lower }}.{{ extension }}';
		{% endif %}
		class {{ name | capitalize }}{% if base_class %} extends {{ base_class | capitalize }}{% endif %} {
			constructor() {
				{%+ if base_class %}super();{% endif %}
			}
		}
		""",
	'Pascal': """\
		unit {{ name | capitalize }};

		interface
		{%if base_class %}
		uses
			{{ base_class | capitalize }};
		{% endif %}
		type
			T{{ name | capitalize }} = class{% if base_class %}(T{{ base_class | capitalize }}){% endif %}
				private
				public
					constructor Create;
					destructor Destroy;
			end;

		implementation

		constructor T{{ name | capitalize }}.Create;
		begin

		end;

		destructor T{{ name | capitalize }}.Destroy;
		begin

		end;

		end.
		""",
	'PHP': """\
		{%- if namespace %}
		namespace {{ namespace | title | replace(' ', '\\\\') }};
		{% endif %}
		{%- if base_class %}
		require '{{ base_path }}{{ base_class | lower }}.{{ extension }}';
		{% endif %}
		class {{ name | capitalize }}{% if base_class %} extends {{ base_class | capitalize }}{% endif %} {
			public function __construct() {
				{%+ if base_class %}parent::__construct();{% endif %}
			}

			public function __destruct() {

			}
		}
		""",
	'Python': """\
		{%- if base_class %}
		from {{ base_path | replace('/', '.') }} import {{ base_class | capitalize }}
		{% endif %}
		{%- if entry_point %}
		import sys
		{% endif %}
		class {{ name | capitalize }}{% if base_class %}({{ base_class | capitalize }}){% endif %}:
			def __init__(self) -> None:
				{%- if base_class %}
				super().__init__()
				{% else %}
				pass
				{% endif %}
			def __del__(self) -> None:
				pass
		{% if entry_point %}

		def main() -> int:
			return 0


		if __name__ == '__main__':
			sys.exit(main())
		{% endif -%}
		"""
}


@dataclass
class ClassCreator:
	"""Creates class files.

	Creates source code files for given language & stores them in the given
	path. A nice heading is also added at the top of each file.

	Parameters
	----------
	base_class: str
		Name of the base class
	base_path: str
		Relative path to the base class
	entry_point: bool
		Create the equivalent of a main file for given language (default: False)
	language: str
		Create file(s) for this language (default: C)
	licence: str
		The licence under which the file(s) are released
	name: str
		Name of the class/unit
	namespace: str
		Add namespace/package if applicable
	path: str
		Relative path to the file(s) from root
	root: str
		Path to directory root
	"""
	base_class: str = ''
	base_path: str = ''
	entry_point: bool = False
	language: str = 'C'
	licence: str = ''
	name: str = ''
	namespace: str = ''
	path: str = ''
	root: str = ''

	@staticmethod
	def _parse(template: str, data: dict, path: str) -> None:
		"""Helper function used to create the file from templates.

		Parameters
		----------
		template: str
			Content of a tamplate
		data: dict
			Values to use when parsing the template
		path: str
			Path to where the result will be written
		"""
		options: dict = {
			'keep_trailing_newline': True,
			'lstrip_blocks': True
		}

		Template(head + dedent(template), **options).stream(data).dump(path)

	def run(self) -> int:
		"""Creates the class file(s).

		Returns
		-------
		int
			Return code
		"""
		extensions: dict[str, str] = {
			'C': 'c',
			'C++': 'cpp',
			'Java': 'java',
			'JavaScript': 'js',
			'Pascal': 'pas',
			'PHP': 'php',
			'Python': 'py'
		}

		# Unhandled languages
		if self.language not in extensions:
			print('Unsupported language')
			return 1

		# Unhandled licences
		if self.licence and self.licence not in ('GPLv3', 'MIT'):
			print('Unsupported licence')
			return 1

		# Read the global .gitconfig file to get author's name
		git = ConfigParser()
		git.read(expanduser('~/.gitconfig'))

		data: dict = self.__dict__
		directory: str = join(self.root, self.path)
		extension: str = extensions[self.language]
		name: str = self.name

		# Create directories as needed
		if directory:
			makedirs(directory, exist_ok=True)

		if self.base_path:
			self.base_path += '/'

		if self.path:
			self.path += '/'

		data['author'] = git['user']['name']
		data['comment'] = '#' if self.language == 'Python' else ' *'
		data['comment_begin'] = '{**' if self.language == 'Pascal' else '# ' if self.language == 'Python' else '/**'
		data['comment_end'] = ' *}' if self.language == 'Pascal' else '#' if self.language == 'Python' else ' */'
		data['extension'] = extension
		data['year'] = date.today().year

		# Parse templates using given data & write the result to a file. These
		# languages have a separate entry point file
		if self.entry_point and self.language in ('C', 'C++', 'JavaScript', 'Pascal', 'PHP'):
			data['name'] = name = 'main' if self.language in ('C', 'C++') else 'index'

			if self.language == 'Pascal':
				data['name'] = name = 'Main'

			self._parse(entry_points[self.language], data, join(directory, f'{name}.{extension}'))

		else:
			# Parse the template using given data & write it to a file. The
			# name of these files is capitalised
			if not self.name:
				if self.language == 'Python' and self.entry_point:
					data['name'] = name = 'main'

				elif self.language == 'Java' and self.entry_point:
					data['name'] = name = 'Main'

				else:
					print('Name is required')
					return 1

			if self.language not in ('C', 'C++', 'JavaScript', 'Python'):
				name = self.name.capitalize()
			self._parse(sources[self.language], data, join(directory, f'{name}.{extension}'))

			# These languages need to produce a second file alongside its source
			if self.language in ('C', 'C++'):
				data['extension'] = extension = extension.replace('c', 'h')
				self._parse(headers[self.language], data, join(directory, f'{self.name}.{extension}'))

		return 0


def main() -> int:
	"""Runs the class creator.

	Returns
	-------
	int
		Return code
	"""
	parser: CustomArgumentParser = CustomArgumentParser(
		description='Class Creator for Programming Languages'
	)

	parser.add_argument(
		'-B',
		'--base-path',
		default='',
		help='relative path to the base class'
	)

	parser.add_argument(
		'-b',
		'--base-class',
		default='',
		help='name of the base class'
	)

	parser.add_argument(
		'-e',
		'--entry-point',
		action='store_true',
		help='create the equivalent of a main file for given language'
	)

	parser.add_argument(
		'-i',
		'--interact',
		action='store_true',
		help='in interactive mode %(prog)s will ask for arguments not provided'
	)

	parser.add_argument(
		'-l',
		'--language',
		choices=('C', 'C++', 'Java', 'JavaScript', 'Pascal', 'PHP', 'Python'),
		default='C',
		help='create file(s) for this language'
	)

	parser.add_argument(
		'-L',
		'--licence',
		choices=('BSD', 'GPLv3', 'MIT'),
		default='',
		help='the licence under which the file(s) are released'
	)

	parser.add_argument(
		'-n',
		'--name',
		help='name of the class/unit'
	)

	parser.add_argument(
		'-N',
		'--namespace',
		default='',
		help='add namespace/package if applicable'
	)

	parser.add_argument(
		'-p',
		'--path',
		default='',
		help='relative path to the file(s) from root'
	)

	parser.add_argument(
		'-r',
		'--root',
		default='',
		help='path to directory root'
	)

	parser.add_argument(
		'-v',
		'--version',
		action='version',
		version='%(prog)s 2.1.1'
	)

	# Run the parser and return the result
	data: Namespace = parser.parse_args()

	# If in interactive mode prompt for the data which wasn't provided
	# This is useful when calling this script from a Makefile
	try:
		if data.interact:
			if not data.name and not (data.entry_point and data.language in ('C', 'C++', 'JavaScript', 'PHP')):
				data.name = input('Name: ')

			if not data.path:
				data.path = input('Path: ')

			if data.language != 'C':
				if not data.base_class:
					data.base_class = input('Base class: ')

				if data.base_class and not data.base_path:
					data.base_path = input('Base path: ')

			if not data.namespace and data.language in ('C++', 'Java', 'PHP'):
				data.namespace = input('Namespace: ')

	except KeyboardInterrupt:
		return 1

	del data.interact

	# Run the class creator
	return ClassCreator(**data.__dict__).run()


if __name__ == '__main__':
	sys.exit(main())
