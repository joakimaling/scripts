#!/usr/bin/python3
"""Parses sessions in a tool to create statistics"""
#                     ___           __  ___
#  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
# / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
# \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
#         https://gitlab.com/carolinealing    /___/
#
# ~/Scripts/parse_sessions.py
#
# Goal
# --------------------------------
# Get total number of sites closed
# - per month
# - per webvol
#

# Common modules
from datetime import datetime
import csv
import json
import os
import sys

home: str = os.environ['HOME']
months_data: dict[str, dict[str, int | list[int]]] = {}
target: str = os.path.join(home, 'Projects/web/tools.local/smartblock/sessions')
webvol_data: list[int] = [0] * 40

for filename in os.listdir(target):
	with open(os.path.join(target, filename), 'r', encoding='utf-8') as file:
		data: dict = json.loads(file.read())

		try:
			# Check if any sites were closed
			if 'executed' in data and 'sites' in data and len(data['sites']) > 0 and data['actions']['redirect']['action'] == 'set':
				time: datetime = datetime.strptime(data['executed'][:-6], '%Y-%m-%dT%H:%M:%S')
				month: str = time.strftime('%B')

				if month not in months_data:
					months_data[month] = {'webvols': [0] * 40, 'total': 0}

				for site in data['sites']:
					if 'vhost' in site:
						webvol: int = int(site['vhost']['webvol'].replace('webvol', '')) - 1
						webvol_data[webvol] += 1
						months_data[month]['webvols'][webvol] += 1
						months_data[month]['total'] += 1

		except KeyboardInterrupt:
			print('Bye')
			sys.exit(1)

		except KeyError:
			print(json.dumps(data, indent=2))

with open('sessions.csv', 'w', encoding='utf-8') as file:
	writer = csv.writer(file)

	writer.writerow([''] + [f'webvol{number}' for number in range(1, 41)] + ['Total'])

	for month, data in months_data.items():
		writer.writerow([month] + [str(item) for item in data['webvols']] + [str(data['total'])])

	writer.writerow(['Total'] + webvol_data)
