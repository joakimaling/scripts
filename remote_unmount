#!/bin/bash
#                     ___           __  ___
#  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
# / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
# \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
#         https://gitlab.com/carolinealing    /___/
#
# ~/Scripts/remote_unmount
#
# Requires: fusermount
#

usage() {
    /usr/bin/cat <<USAGE
Usage: $(/usr/bin/basename $0) [options]

Unmounts given mount path previously mounted.

Options:
  -f, --force               Force deletion of mount path
  -p, --path PATH           Mount path to be unmounted. Required
  -r, --root ROOT           The root path. Required. Default: ${_root_path}

Required parameters must not be empty.
USAGE
    exit 1
}

# Default values
_root_path="${HOME}/Projects/mounts"

# Starting values
force=false
root_path="$_root_path"

# Parse arguments
while [ $# -gt 0 ]; do
  case "$1" in
    -f|--force) force=true ;;
    -p|--path) shift; path="$1" ;;
    -r|--root) shift; root_path="$1" ;;
    *) usage ;;
  esac
  shift
done

# Check required flags - exit if one is missing
[ -z "${path}" ] || [ -z "${root_path}" ] && usage

# Prepend the root path
path="${root_path}/${path}"

# Perform unmount
/usr/bin/fusermount -q -u "${path}"

# Try to remove local directory
if [ $? -eq 0 ] || [ $force = true ]; then
  /bin/rmdir "${path}"
  exit 0
fi

# Signal error
echo "Can't unmount ${path}"
exit 1
