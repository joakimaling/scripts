#!/usr/bin/python3
"""Parses templates for various purposes"""
#                     ___           __  ___
#  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
# / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
# \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
#         https://gitlab.com/carolinealing    /___/
#
# ~/Scripts/custom/parse_templates.py
#

# Common modules
from argparse import Namespace
from dataclasses import dataclass, field
from os import makedirs
from os.path import exists, expanduser, join
import sys

# Third-party modules
from jinja2 import Environment, FileSystemLoader
from yaml import YAMLError, safe_load # type:ignore

# Utility modules
from tools.parser import CustomArgumentParser
from tools.log import Log

# Setting up logs
log = Log(__file__, handlers=('console',)) # pylint: disable=superfluous-parens


@dataclass
class TemplateParser:
	"""Parses templates.

	Parses templates in Jinja format according to specified values and flags,
	and stores the resulting files in a specified directory.

	Parameters
	----------
	directory: str
		Name of the directory in which the resulting files will be placed
	file_type: str
		Type of file. Used if language 'TeX' was specified (default: report)
	language: str
		Programming language (default: C)
	licence: str
		Licence to be included. Used if language 'TeX' was not specified
	source: str
		Directory where the templates are located (default: ~/Templates)
	target: str
		Directory where the new directory will be created (default: ~/Projects)
	values: dict
		Path to a YAML file of values to be used with the templates
	"""
	directory: str
	file_type: str = 'report'
	language: str = 'C'
	licence: str = ''
	source: str = expanduser('~/Templates')
	target: str = expanduser('~/Projects')
	values: dict = field(default_factory=dict)

	def _parse(self, source_file: str, target_file: str = '') -> None:
		"""
		Helper function when parsing each template.
		"""
		Environment(loader=FileSystemLoader(self.source)) \
			.get_template(source_file).stream(self.values) \
			.dump(join(self.target, self.directory, target_file or source_file))

	def run(self) -> int:
		"""Parses the templates.

		Returns
		-------
		int
			Return code
		"""
		path: str = join(self.target, self.directory)

		# Create the path if it doesn't exist
		if not exists(path):
			makedirs(path)

		if self.language == 'TeX':
			for template in (f'{self.file_type}.tex', 'bibliography.bib'):
				log.info('Creating TeX file %s', template)
				self._parse(template)

		else:
			# Makefile
			if self.language in ('C', 'C++', 'Java', 'Pascal'):
				log.info('Creating Makefile for %s', self.language)
				self._parse(f'Makefile-{self.language}.j2', 'Makefile')

			# package.js
			elif self.language == 'JavaScript':
				log.info('Creating JavaScript file package.json')
				self._parse('package.json.j2', 'package.json')

			# LICENSE.md
			if self.licence:
				log.info('Creating %s licence', self.licence)
				self._parse(f'LICENSE-{self.licence}.md.j2', 'LICENSE.md')

			# README.md
			log.info('Adding a README file')
			self._parse('README.md.j2', 'README.md')

		# .editorconfig
		# .gitignore
		for file in ('.editorconfig', '.gitignore'):
			log.info('Creating %s', file)
			self._parse(f'{file}.j2', file)

		return 0


def main() -> int:
	"""Runs the template parser.

	Returns
	-------
	int
		Return code
	"""
	parser: CustomArgumentParser = CustomArgumentParser(
		description='Template Parser'
	)

	parser.add_argument(
		'directory',
		help='name of the directory in which the resulting files will be placed'
	)

	parser.add_argument(
		'-f',
		'--file_type',
		choices=('article', 'letter', 'report'),
		default='report',
		help='type of file. used if language \'TeX\' was specified'
	)

	parser.add_argument(
		'-l',
		'--language',
		choices=('C', 'C++', 'Java', 'JavaScript', 'Pascal', 'PHP', 'Python', 'TeX'),
		default='TeX',
		help='programming language'
	)

	parser.add_argument(
		'-L',
		'--licence',
		choices=('GPLv3', 'MIT'),
		default='',
		help='licence to be included. used if language \'TeX\' was not specified'
	)

	parser.add_argument(
		'-s',
		'--source',
		default='~/Templates',
		help='directory where the templates are located'
	)

	parser.add_argument(
		'-t',
		'--target',
		default='~/Projects',
		help='directory where the new directory will be created',
	)

	parser.add_argument(
		'-v',
		'--values',
		default='',
		help='path to a YAML file of values to be used with the templates'
	)

	parser.add_argument(
		'-V',
		'--version',
		action='version',
		version='%(prog)s 0.1.3'
	)

	# Run the parser and return the result
	data: Namespace = parser.parse_args()

	# Read the file provided containing values for the parser. The file is in
	# YAML format. Exit if there's an exception
	if data.values:
		try:
			with open(data.values, 'r', encoding='utf-8') as file:
				data.values = safe_load(file)

		except (FileNotFoundError, YAMLError):
			return 1

	# Run the parser
	return TemplateParser(**data.__dict__).run()


if __name__ == '__main__':
	sys.exit(main())
