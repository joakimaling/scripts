#!/usr/bin/python3
"""Backs up selected files to a local Samba share"""
#                     ___           __  ___
#  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
# / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
# \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
#         https://gitlab.com/carolinealing    /___/
#
# ~/Scripts/backup_files.py
#

# Common modules
from os.path import exists, join
import os
import sys

# Third-party modules
from dotenv import load_dotenv

# Utility modules
from tools.log import Log
from tools.process import run

# Import secrets
load_dotenv()

# Setting up logs
log = Log(__file__)

# Code to return after the script exits
exit_code: int = 0

# Share to which the files are copied
share: str = os.getenv('BACKUP_SHARE') # type: ignore

# Directories to backup to the share
directories: tuple[str, ...] = (
	'Documents',
	'Pictures'
)

# This file describes files which shan't be backed up
log_path: str = log.get_path()
pattern: str = '\.env|\.mypy_cache|vendor'

try:
	if exists(share):
		# The list of directories which are to be backed up are looped through
		for directory in directories:
			source: str = join(os.environ['HOME'], directory)
			target: str = join(share, directory)

			# Create the target folder if it doesn't exist
			os.makedirs(target, exist_ok=True)

			# Use Rsync to perform backups mirroring what's in the source on
			# the target
			code, error, _ = run(
				'/usr/bin/rsync -AXaqu --delete --delete-excluded '
				f'--exclude={pattern} --log-file={log_path} '
				f"'{source}' '{target}'"
			)

			# The final exit code will be the highest value, meaning the script
			# will return an error value if any of the backups failed
			exit_code = max(exit_code, code)

			# Log the failures, including their cause
			if code != 0:
				log.error('Backup of %s failed. %s', directory, error)

			else:
				log.info('Backup of %s successful', directory)

# If the share hasn't been mounted (the directory is missing) the script will
# log it & exit
except FileExistsError:
	log.error('Backup failed. Share not mounted?')
	exit_code = 1 # pylint: disable=invalid-name

sys.exit(exit_code)
