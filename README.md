# Scripts

> A collection of scripts

[![Licence][licence-badge]][licence-url]

A collection of scripts for various purposes to increase workflow or to be used
by other projects.

## Installation

Simply clone it like this:

```sh
git clone git@gitlab.com:carolinealing/scripts.git
```

## Licence

Released under MIT. See [LICENSE.md][licence-url] for more.

Coded with :heart: by [Caroline Åling][user-url].

[licence-badge]: https://img.shields.io/gitlab/license/carolinealing%2Fscripts
[licence-url]: LICENSE.md
[user-url]: https://gitlab.com/carolinealing
