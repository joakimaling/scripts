#!/usr/bin/python3
"""Scans files in search of viruses"""
#                     ___           __  ___
#  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
# / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
# \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
#         https://gitlab.com/carolinealing    /___/
#
# ~/Scripts/scan_files.py
#
# This script logs to /var/log/clamav/clamd.log
#

# Common modules
from os.path import expanduser

# Utility modules
from tools import mail, notify, process
from tools.log import Log

# Setting up logs
log = Log(__file__)

# Path to the directory downloaded files
target: str = expanduser('~/Downloads')

# Scan the target
code, error, output = process.run(
    f'/usr/bin/clamdscan -i -m --fdpass --no-summary {target}'
)

# Maps the return code to an appropriate text string
content: list[str] = [
    'No threats found',
    output,
    error
]

# Notify the user about the outcome
if code > 0:
	log.error(content[code])
	mail.send(content[code], 'ClamAV')
	notify.show(content[code], 'ClamAV', level='critical')

else:
	log.info(content[code])
	notify.show(content[code], 'ClamAV')
