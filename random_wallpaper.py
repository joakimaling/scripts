#!/usr/bin/python3
"""Sets a random wallpaper on the desktop"""
#                     ___           __  ___
#  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
# / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
# \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
#         https://gitlab.com/carolinealing    /___/
#
# ~/Scripts/random_wallpaper.py
#
# https://github.com/theistic/nordic-wallpapers
#

# Common modules
from glob import glob
from os.path import expanduser
import random
import sys

# Utility modules
from tools.log import Log
from tools.process import run


def quote(path: str) -> str:
	"""Quotes a path to a file

	Parameters
	----------
	path: str
		a path to a file

	Returns
	-------
	str
		a quoted path to file
	"""
	return f"'{path}'"


# Setting up logs
log = Log(__file__)

# Code to return after the script exits
exit_code: int = 0

# Different image extensions present in the backgounds directory
extensions: tuple[str, ...] = ('jpeg', 'jpg', 'png')

# List of image files to use as wallpapers
files: list[str] = []

# The keys to set in order to change the wallpapers/screensaver
keys: tuple[str, ...] = (
	'/org/gnome/desktop/background/picture-uri-dark',
	'/org/gnome/desktop/background/picture-uri',
	'/org/gnome/desktop/screensaver/picture-uri'
)

# Path to the backgrounds directory
source: str = expanduser('~/.local/share/backgrounds/nord/')

# Get the current wallpaper
_, _, output = run(f'/usr/bin/dconf read {keys[0]}')

# Get a list of wallpapers
for extension in extensions:
	files.extend(glob(f'{source}/[0-9]*.{extension}'))

# Remove the wallpaper already in use
files = [item for item in files if quote(item) != output]

# Randomly pick a background to use as wallpaper
file: str = random.choice(files)

# The value needs to be quoted
value: str = quote(file)

# Set the next wallpaper
for key in keys:
	code, error, output = run(f'/usr/bin/dconf write {key} {value}')

	# The final exit code will be the highest value, meaning the script
	# will return an error value if any of the backups failed
	exit_code = max(exit_code, code)

	# Log the failures, including their cause
	if code != 0:
		log.error("Failed setting '%s' as wallaper. %s", file, error)

if exit_code == 0:
	log.info("Set '%s' as wallaper successfully", file)

sys.exit(exit_code)
