#!/usr/bin/python
"""Put secret in to the system's clipboard"""
#                     ___           __  ___
#  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
# / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
# \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
#         https://gitlab.com/carolinealing    /___/
#
# ~/Scripts/copy_secret.py
#
# Run `op account add` in a terminal to set up
# https://developer.1password.com/docs/cli/app-integration/
#

# Common modules
import os
import sys

# Third-party modules
from dotenv import load_dotenv

# Utility modules
from tools import notify, process
from tools.log import Log

# Import secrets
load_dotenv()

# Setting up logs
log = Log(__file__)

# Which item to fetch from which vault
item: str = os.getenv('PASSWORD_ITEM') # type: ignore
vault: str = os.getenv('PASSWORD_VAULT') # type: ignore

# Variables
code: int
message: str = 'Secret failed to be copied'
output: str

# Get the secret using 1Password
code, _, output = process.run(
	f'op item get {item} --fields label=password --vault {vault}'
)

# Put the secret into the system's clipboard, if it was successfully fetched
# in the previous command
if code == 0:
	log.info('Secret successfully fetched')
	code = process.run_background('/usr/bin/xclip -sel clip', output.strip())
	#code = process.run_background('/usr/bin/wl-copy', output.strip())

	if code == 0:
		log.info('Secret successfully copied')
		message = 'Secret copied to clipboard' # pylint: disable=invalid-name

	else:
		log.error('Copying secret failed')

else:
	log.error('Fetching secret failed')

# Display a small notification
notify.show(message, 'Copy secret')

# Exit properly
sys.exit(code)
