#!/usr/bin/python3
"""Updates the address of a domain"""
#                     ___           __  ___
#  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
# / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
# \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
#         https://gitlab.com/carolinealing    /___/
#
# ~/Scripts/update_domain.py
#

# Common modules
from urllib.error import URLError
from urllib.request import Request, urlopen
import json
import os
import sys

# Third-party modules
from dotenv import load_dotenv

# Import secrets
load_dotenv()


class Domain:
	"""Updates a domain

	Updates the external IP address for a given domain. The external IP address
	is fetched from on of a list with online resources.
	"""
	services: list[str] = [
		'https://ipv4.icanhazip.com/',
		'https://api.ipify.org/',
		'http://ipinfo.io/ip'
	]

	@staticmethod
	def send_request(token: str, target: str, method: str = 'get', data: dict = {}):
		"""Sends a request

		Parameters
		----------
		token: str
			Token used to authenticate the user
		target: str
			Which endpoint to use when communicating with the server
		method: str
			Which HTTP method to use
		data: dict
			Data to send
		"""
		request: Request = Request(
			f'https://api.cloudflare.com/client/v4/{target}',
			data=data,
			method=method,
			headers={
				'Authorization': f'Bearer {token}',
				'Content-Type': 'application/json'
			}
		)

		with urlopen(request) as response:
			return json.loads(response.read())

	def update(self) -> int:
		"""Updates a domain

		Returns
		-------
		int
			Exit code
		"""
		address: str
		token: str = os.getenv('DOMAIN_TOKEN') # type: ignore
		zone: str = os.getenv('DOMAIN_ZONE') # type: ignore

		for service in self.services:
			try:
				with urlopen(service) as response:
					address = response.read().decode()
					break

			except URLError:
				continue
		else:
			return 1

		# Get record
		data: dict = self.send_request(token, f'zones/{zone}dns_records/')

		if data['success']:
			record = filter(lambda item: item['zone_name'] == zone, data['result'])

			# Make the update
			data = self.send_request(token, f'zones/{zone}dns_records/{record}', 'put',	{
				'content': address,
				'name': '@',
				'proxied': True,
				'ttl': 1
			})

		return 0


def main() -> int:
	"""Updates a domain

	Returns
	-------
	int
		Exit code
	"""
	return Domain().update()


if __name__ == '__main__':
	sys.exit(main())
