#!/usr/bin/python3
"""Generates a number of fairly complex passwords"""
#                     ___           __  ___
#  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
# / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
# \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
#         https://gitlab.com/carolinealing    /___/
#
# ~/Scripts/generate_passwords.py
#

# Common modules
from argparse import Namespace
from dataclasses import dataclass
from random import choice
import string
import sys

# Utility modules
from tools.parser import CustomArgumentParser


@dataclass
class PasswordGenerator():
	"""Generates passwords

	Generates a given number of passwords of given complexity. Useful when just
	wanting to quickly create some in the terminal.

	Parameters
	----------
	amount: int
		How many passwords to create
	length: int
		Length, in characters, of each password
	symbols: bool
		Whether to include symbols
	"""
	amount: int = 8
	length: int = 32
	symbols: bool = True

	def run(self) -> int:
		"""Generates passwords

		Generates given number of passwords of given length of fairly good
		complexity. The passwords contain letters, numbers and symbols.

		Returns
		-------
		int
			Exit code
		"""
		characters: str = string.ascii_letters + string.digits
		result: str = ''

		# Inlcude symbols in the passwords?
		if self.symbols:
			characters += string.punctuation

		# Generate a bunch of them
		for index in range(self.amount):
			result = ''.join(choice(characters) for x in range(self.length))
			print(f'{index + 1}: {result}')

		return 0


def main() -> int:
	"""Generates passwords

	Returns
	-------
	int
		Exit code
	"""
	parser: CustomArgumentParser = CustomArgumentParser(
		description='Simple Password Generator'
	)

	# Number of passwords
	parser.add_argument(
		'-n', '--amount',
		default=8,
		help='Number of passwords to generate',
		type=int
	)

	# Length of password
	parser.add_argument(
		'-l', '--length',
		default=32,
		help='Length, in characters, of each password',
		type=int
	)

	# Include symbols
	parser.add_argument(
		'-s', '--symbols',
		action='store_true',
		help='Whether to include non-alphanumeric symbols'
	)

	arguments: Namespace = parser.parse_args()

	return PasswordGenerator(**arguments.__dict__).run()


if __name__ == '__main__':
	sys.exit(main())
