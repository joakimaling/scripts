#!/usr/bin/python3
"""Creates a coding project"""
#                     ___           __  ___
#  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
# / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
# \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
#         https://gitlab.com/carolinealing    /___/
#
# ~/Scripts/create_project.py
#

# Common modules
from argparse import Namespace
from dataclasses import dataclass
from datetime import date
from os import environ, listdir, makedirs
from os.path import join
import sys

# Third-party modules
from git import Repo
from gitlab import Gitlab
from gitlab.exceptions import GitlabCreateError

# Utility modules
from create_class import ClassCreator
from parse_templates import TemplateParser
from tools.parser import CustomArgumentParser
from tools.log import Log

# Setting up logs
log = Log(__file__, handlers=('console',)) # pylint: disable=superfluous-parens


@dataclass
class ProjectCreator:
	"""Creates a boilerplate for a project & makes an initial commit to GitLab.

	Parameters
	----------
	name: str
		Name of the project
	token: str
		Personal access token for the GitLab account
	contribute: bool
		Allow others to contribute to the project (default: False)
	description: str
		Description for the project
	language: str
		Programming language (default: C)
	licence: str
		Include given licence (default: MIT)
	private: bool
		Make the project private on GitLab (default: False)
	"""
	name: str
	token: str
	contribute: bool = False
	description: str = ''
	language: str = 'C'
	licence: str = ''
	private: bool = False

	def run(self) -> int:
		"""Creates the project.

		Returns
		-------
		int
			Return code
		"""
		# Establish a connection to GitLab (requires a personal access token)
		gitlab: Gitlab = Gitlab(private_token=self.token)
		gitlab.auth()

		try:
			# Create a project on GitLab & set its visibility
			log.info('Creating "%s" on GitLab', self.name)
			result = gitlab.projects.create({
				'description': self.description,
				'name': self.name.capitalize,
				'path': self.name.lower,
				'visibility': 'private' if self.private else 'public'
			})

		except GitlabCreateError as error:
			log.error(error)
			return 1

		data: dict = self.__dict__
		home: str = environ['HOME']

		data['project_path'] = join(home, 'Projects', self.name)
		data['templates_path'] = join(home, 'Templates')
		data['year'] = date.today().year

		document_path: str = join(home, 'Documents', 'Notes', 'Projects')
		project_path: str = data['project_path']

		# Clone the newly-created project from GitHub
		log.info('Cloning "%s" to %s', self.name, project_path)
		project: Repo = Repo.clone_from(result.ssh_url_to_repo, project_path)

		# Add some more data for the templates
		data['author'] = result.owner['name']
		data['email'] = project.config_reader().get('user', 'email')
		data['slug'] = result.path
		data['user'] = result.owner['username']

		# Create files from templates pertaining to the specified language
		log.info('Copying over templates (%s)', self.language)
		TemplateParser(
			directory=self.name,
			language=self.language,
			licence=self.licence,
			values=data
		).run()

		source_path: str = project_path

		# Create necessary directories for these languages
		if self.language in ('C', 'C++', 'Java', 'Pascal'):
			makedirs(join(project_path, 'resources'))
			makedirs(join(project_path, 'source'))
			source_path = join(project_path, 'source')

		# Create the first entry point source file
		log.info('Creating entry point file (%s)', self.language)
		ClassCreator(
			entry_point=True,
			language=self.language,
			licence=self.licence,
			name=self.name,
			root=source_path
		).run()

		# Create a directory for project notes
		makedirs(document_path, exist_ok=True)

		# Create a note for a list of ideas & useful resources
		with open(join(document_path, f'{self.name}.md'), 'w', encoding='utf-8') as file:
			links: dict[str, str] = {
				'C': 'https://en.cppreference.com/w/c',
				'C++': 'https://en.cppreference.com/w/cpp',
				'Java': 'https://docs.oracle.com/en/java/javase/23/docs/api/index.html',
				'JavaScript': 'https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference',
				'Pascal': 'https://www.freepascal.org/docs.var',
				'PHP': 'https://www.php.net/',
				'Python': 'https://docs.python.org/3/library/'
			}

			file.write(f'Language: [{self.language}]({links[self.language]})\n')
			file.write('Libraries: []()\n\n')
			file.write(f'# Description\n{self.description}\n')
			file.write('# Ideas\n- [ ] \n')
			file.write('# Resources\n- []()\n')

		# Get a list of all files to be added
		files: list[str] = listdir(project_path)
		files.remove('.git')

		# Add files to the project index
		log.info('Adding %s to index', ', '.join(files))
		project.index.add(files)

		# Make the first commit
		log.info('Making an initial commit')
		project.index.commit('Initial commit')

		# Push the project to GitLab
		log.info('Pushing project "%s" to GitLab', self.name)
		project.remotes.origin.push()

		return 0


def main() -> int:
	"""Runs the project creator.

	Returns
	-------
	int
		Return code
	"""
	parser: CustomArgumentParser = CustomArgumentParser(
		description='Project Boilerplate Creator'
	)

	parser.add_argument(
		'name',
		help='name of the project'
	)

	parser.add_argument(
		'token',
		help='personal access token for the GitLab account',
		required=True
	)

	parser.add_argument(
		'-c',
		'--contribute',
		action='store_true',
		help='allow others to contribute to the project'
	)

	parser.add_argument(
		'-d',
		'--description',
		help='description for the project'
	)

	parser.add_argument(
		'-l',
		'--language',
		choices=('C', 'C++', 'Java', 'JavaScript', 'Pascal', 'PHP', 'Python'),
		default='C',
		help='programming language'
	)

	parser.add_argument(
		'-L',
		'--licence',
		choices=('BSD', 'GPLv3', 'MIT'),
		default='',
		help='include given licence'
	)

	parser.add_argument(
		'-p',
		'--private',
		action='store_true',
		help='make the project private on GitLab'
	)

	parser.add_argument(
		'-v',
		'--version',
		action='version',
		version='%(prog)s 2.1.2'
	)

	# Run the parser and return the result
	data: Namespace = parser.parse_args()

	# Run the project creator
	return ProjectCreator(**data.__dict__).run()


if __name__ == '__main__':
	sys.exit(main())
