#!/usr/bin/python3
#                     ___           __  ___
#  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
# / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
# \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
#         https://gitlab.com/carolinealing    /___/
#
# ~/Scripts/create_tickets.py
#

# Generic libraries
from dataclasses import dataclass
from urllib.error import URLError
from urllib.request import Request, urlopen


@dataclass
class TicketCreator:
	"""
	This class describes a ticket creator.
	"""
	project_name: str

	def _send_request(self, endpoint: str, data: dict):
		"""
		Sends a request.

		:param      endpoint:  The endpoint
		:type       endpoint:  str
		:param      data:      The data
		:type       data:      dict
		"""
		request: Request = Request(
			f'https://projects.penguinlair.com/{endpoint}',
			data={
				name: self.project_name
			},
			method='post'
		)

		try:
			with urlopen(request) as response:
				return response.statusCode() == 200

		except URLError as error:
			print(error)

	def create_project(self, description: str = ''):
		"""
		Creates a project.

		:param      description:  The description
		:type       description:  str
		"""
		self._send_request('newProject', {
			description: description
		})

	def create_ticket(self, name: str):
		"""
		Creates a ticket.

		:param      name:  The name
		:type       name:  str
		"""
		self._send_request('newProject', {
			name: name
		})
