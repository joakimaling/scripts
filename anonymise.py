#!/usr/bin/python3
"""Anonymises deleted people in a Slack dump"""
#                     ___           __  ___
#  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
# / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
# \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
#         https://gitlab.com/carolinealing    /___/
#
# ~/Scripts/anonymise.py
#

# Common modules
from os import environ
from os.path import join
import re

# Files to operate upon
source_file: str = join(environ['HOME'], 'Downloads/slackdump2html-main/data/users-original.txt')
target_file: str = join(environ['HOME'], 'Downloads/slackdump2html-main/data/users.txt')

with open(source_file, 'r', encoding='utf-8') as source:
	data = source.read()

	# Skip the first two rows & but take each row after that
	rows = data.split('\n')[2:]

	with open(target_file, 'w', encoding='utf-8') as target:
		target.write('Name                   ID           Bot?  Deleted?  Restricted?\n\n')

		for row in rows:
			items: list[str] = re.split(r'\s+', row)

			name: str = items[0]
			identity: str = items[1]

			is_bot: bool = 'bot' in items[2:]
			is_deleted: bool = 'deleted' in items[2:]
			is_restricted: bool = 'restricted' in items[2:]

			# Anonymise the user if they were deleted (when they quit work)
			if is_deleted and not is_bot:
				name = 'deleted.user'

			bot: str = 'bot' if is_bot else ''
			deleted: str = 'deleted' if is_deleted else ''
			restricted: str = 'restricted' if is_restricted else ''
			target.write(f'{name:<23}{identity:<13}{bot:<6}{deleted:<10}{restricted}\n')
