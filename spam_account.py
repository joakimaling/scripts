#!/usr/bin/python3
"""Sends a number of e-mails to an e-mail account"""
#                     ___           __  ___
#  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
# / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
# \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
#         https://gitlab.com/carolinealing    /___/
#
# ~/Scripts/spam_account.py
#

# Common modules
from argparse import ArgumentParser, Namespace
from email.header import Header
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import formatdate
from functools import partial
from os.path import splitext
from multiprocessing import Pool, cpu_count
from smtplib import SMTP, SMTPException
import os
import sys

# Third-party modules
from dotenv import load_dotenv
import lorem # type: ignore # pylint: disable=E0401

# Utility modules
from tools.log import Log

# Import secrets
load_dotenv()

# Server & authentication settings
mail_host: str = os.getenv('MAIL_HOST') # type: ignore
mail_password: str = os.getenv('MAIL_PASSWORD') # type: ignore
mail_port: int = int(os.getenv('MAIL_PORT')) # type: ignore
mail_user: str = os.getenv('MAIL_USER') # type: ignore

# E-mail settings ('mail_to' is the victim)
mail_to: str = os.getenv('MAIL_ADDRESS') # type: ignore

# Setting up logs
log = Log(__file__, handlers=('console',)) # pylint: disable=superfluous-parens


def get_body() -> str:
	"""Creates a random number of paragraphs.

	Returns
	-------
	str
		a string of a number of paragraphs
	"""
	return lorem.get_paragraph(count=(10, 50))


def get_sender() -> str:
	"""Creates a random e-mail address.

	Returns
	-------
	str
		an e-mail address
	"""
	domain: str = lorem.get_word()
	username: str = lorem.get_word()
	tops: tuple[str, ...] = ('com', 'cz', 'fi', 'no', 'org', 'rs', 'se', 'sk')
	top_domain: str = lorem.get_word(pool=tops)

	return f'{username}@{domain}.{top_domain}'


def get_subject() -> str:
	"""Creates a random number of words.

	Returns
	-------
	str
		a string of a few words
	"""
	return lorem.get_word(count=(2, 5))


def spam(attachment: str = '', amount: int = 10) -> None:
	"""Sends a number of e-mails and, optionally, a file.

	Sends a given number of e-mails to an e-mail account. The e-mail contains
	a randomised subject and body. Optionally, a file may be attachmed to add
	some size to it.

	Parameters
	----------
	attachment: str
		a path to the file to be attached
	amount: int
		the number of times to repeat sending an e-mail

    """
	while amount > 0:
		sender: str = get_sender()
		subject = Header(get_subject(), 'utf-8')

		content = MIMEMultipart()
		content['Date'] = formatdate(localtime=True)
		content['From'] = sender
		content['Subject'] = str(subject)
		content['To'] = mail_to

		# Attach body
		content.attach(MIMEText(get_body(), 'plain', 'utf-8'))

		# Attach file
		with open(attachment, 'rb') as file:
			data: MIMEApplication = MIMEApplication(
			file.read(),
			_subtype=splitext(attachment)[1]
		)

		data.add_header('Content-Disposition', 'attachment', filename=attachment)
		content.attach(data)

		try:
			with SMTP(mail_host, port=mail_port, timeout=10) as mailer:
				if mail_port > 25:
					mailer.starttls()
					mailer.login(mail_user, mail_password)

				mailer.sendmail(sender, mail_to, content.as_string())

				log.info("Sent '%s' form %s to %s", content['Subject'], sender, mail_to)

		except SMTPException as error:
			log.error("Couldn't send '%s' from %s to %s: %s", subject, sender, mail_to, error)

		amount -= 1


def split(number: int, chunks: int) -> list[int]:
	"""Splits a number and stores its terms in a list.

	This functions aims to divide 'number' by 'chunks' and to fill a list with
	the result repeated 'chunk' times. The final item in the list is the
	remainder of the division. This way we create a list where the sum of all
	items is 'number'. This is a helper function to spread the load across a
	number of processes.

	Parameters
	----------
	number: int
		the divisor, the number to be divided
	chunks: int
		the dividend, the number of items in the list

	Returns
	-------
	list[int]
		a list of integers
	"""
	result: list = [number // chunks] * chunks

	if number % chunks > 0:
		result += [number % chunks]

	return result


def main(data: Namespace) -> int:
	"""Runs the spammer function in multiple processes.

	Reads the arguments provided by the user and runs the spamming function in
	given number of processes.

	Parameters
	----------
	data: Namespace
		a object carrying the values entered on the command-line

	Returns
	-------
	int
		the exit code, 1 if there was a time out error, 0 otherwise
	"""
	if cpu_count() < data.processes:
		raise ValueError(
			'The amount of processes exceeds the amount of available processor cores'
		)

	# Avoid creating empty lists fed to the processes. Let it be at least one
	# e-mail per process
	data.processes = min(data.amount, data.processes)

	try:
		with Pool(processes=data.processes) as pool:
			pool.map(
			partial(spam, data.attachment),
			split(data.amount, data.processes)
		)

	except TimeoutError:
		log.error('Process timed out')
		return 1

	return 0


if __name__ == '__main__':
	parser: ArgumentParser = ArgumentParser(
		description='E-mail spammer for testing purposes'
	)

	# Path to a file to attach
	parser.add_argument(
		'-a', '--attach',
		dest='attachment',
		help='File to attach to the e-mails'
	)

	# The number of e-mails with which to spam an account
	parser.add_argument(
		'-n', '--emails',
		default=10,
		dest='amount',
		help='Number of e-mails',
		type=int
	)

	# The number of processes among which to spread the work
	parser.add_argument(
		'-p', '--processes',
		default=1,
		help='Number of processes',
		type=int
	)

	# Exit the script using the returned exit code
	sys.exit(main(parser.parse_args()))
