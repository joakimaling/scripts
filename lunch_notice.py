#!/usr/bin/python3
"""Displays a menu as a notification before lunch"""
#                     ___           __  ___
#  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
# / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
# \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
#         https://gitlab.com/carolinealing    /___/
#
# ~/Scripts/lunch_notice.py
#

# Common modules
from datetime import datetime
import re
import sys

# Third-party modules
from bs4 import BeautifulSoup # type:ignore
from pymupdf import Document # type:ignore
from requests import get # type:ignore

# Utility modules
from tools import notify

# Exit code & resulting message
code: int = 1
message: list[str] = []

# Get details of current date for use with the functions
days: tuple[str, ...] = ('Sön', 'Mån', 'Tis', 'Ons', 'Tors', 'Fre', 'Lör')
day, month, week, year = datetime.now().strftime('%w %m %-V %Y').split()


def brasseriestadsparken() -> tuple[str, ...]:
	"""Rerturns today's special from Brasserie Stadsparken

	Brasserie Stadsparken puts their menu in a PDF document which has to be
	parsed.

	Returns
	-------
	tuple[str, ...]
		Restaurant name and today's special
	"""
	data: str = ''
	found: bool = False

	resource: str = f'https://usercontent.one/wp/brasseriestadsparken.se/wp-content/uploads/{year}/{month}/Lunch-pa-Parken-V-{week}.pdf'

	for suffix in range(2, 5):
		with get(resource, timeout=10) as response:
			if response.status_code != 200:
				resource = f'https://usercontent.one/wp/brasseriestadsparken.se/wp-content/uploads/{year}/{month}/Lunch-pa-Parken-V-{week}{suffix}.pdf'
				continue

			document = Document(stream=response.content)

			for line in document[0].get_text('dict')['blocks'][1]['lines']:
				text: str = line['spans'][0]['text']

				if found:
					found = False
					data = text

				if days[int(day)].upper() in text:
					found = True

			break


	return (
		'Brasserie Stadsparken',
		re.sub(r'\d+\s*kr\s*$', '', data) # Remove price
	)


def cafegranden() -> tuple[str, ...]:
	"""Rerturns today's special from Café Gränden

	Café Gränden puts their menu on a web page. It's easily parsed markup.

	Returns
	-------
	tuple[str, ...]
		Restaurant name and today's special
	"""
	data: str = ''
	index: int = 0
	resource: str = 'https://cafegranden.se/dagens-lunch/'

	with get(resource, timeout=10) as response:
		if response.status_code == 200:
			markup = BeautifulSoup(response.content, 'html.parser')
			items = markup.find(class_='lunch_menu').contents
			items = list(filter(lambda item: '\n' != item, items))

			while index < len(items):
				data = items[index].find('h3').string

				if re.match(fr'{days[int(day)]}', data):
					data = items[index + 1].find(class_='td_title').string.strip()
					break

				index += 2

	return (
		'Café Gränden',
		data
	)

for source in (brasseriestadsparken, cafegranden):
	name, lunch = source()
	message.append(f'<b>{name}:</b> {lunch}')

# Display a small notification
code = notify.show('\r'.join(message), 'Lunch time')

sys.exit(code)
