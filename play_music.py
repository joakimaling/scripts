#!/usr/bin/python3
"""Plays music from YouTube for focus"""
#                     ___           __  ___
#  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
# / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
# \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
#         https://gitlab.com/carolinealing    /___/
#
# ~/Scripts/play_music.py
#
# Requires: pafy, vlc
#

# Common modules
from argparse import ArgumentParser
import sys

# Third-party modules
import pafy
import vlc


def get_resource(identifier: str) -> str:
	"""?

	Parameters
	----------
	identifier: str
		?

	Returns
	-------
	str
		?
	"""
	video = pafy.new(f'https://www.youtube.com/watch?v={identifier}')
	return video.getbest().url


def start(identifier: str) -> None:
	"""?

	Parameters
	----------
	identifier: str
		?
	"""
	instance = vlc.Instance()
	player = instance.media_player_new()
	media = instance.media_new(get_resource(identifier))
	media.get_mrl()
	player.set_media(media)
	player.play()


#def main() -> int:
#    parser: ArgumentParser = ArgumentParser()

#    parser.add_argument(
#        '-c', '--clip',
#        help=''
#    )

#    parser.add_argument(
#        '-s', '--start',
#        help=''
#    )

#    arguments: Namespace = parser.parse_args()

#    if arguments.start:
#        start()


#    return 0


#if __name__ == '__main__':
#    sys.exit(main())


start('DMc641-k9B8')
