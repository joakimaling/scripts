#!/usr/bin/python3
"""Moves files to directories matching their extension"""
#                     ___           __  ___
#  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
# / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
# \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
#         https://gitlab.com/carolinealing    /___/
#
# ~/Scripts/organise_files.py
#

# Common modules
from os import environ, makedirs, rename, walk
from os.path import isfile, join, splitext
from re import IGNORECASE, search

# Utility modules
from tools.log import Log

# Setting up logs
log = Log(__file__)

# Path to home & the Downloads directory
home: str = environ['HOME']
source: str = join(home, 'Downloads')

# Files matching the regular expression in the value will be moved to the
# directory denoted by its key
targets: dict[str, str] = {
	'Images': r'\.(bin|tgz)$',
	'Pictures': r'\.(jpe?g|png)$',
	'Public': r'\.(pdf|pptx?)$',
}

# Move every file within the Downloads directory matching the expression in to
# its target directory
for root, _, files in walk(source):
	for target, expression in targets.items():
		# Create the target folder if it doesn't exist
		makedirs(join(home, target), exist_ok=True)

		for file in files:
			if search(expression, file, flags = IGNORECASE):
				number: int = 0
				suffix: str = ''
				name, extension = splitext(file)
				target_file: str = join(home, target, name)

				# If there's a file with the same name find the next available
				# suffix to attach to it in order to save it with a unique name
				while isfile(f'{target_file}{suffix}{extension}'):
					suffix = f'-{number}'
					number += 1

				log.info("Moving '%s' to '%s'", join(root, file), f'{target_file}{suffix}{extension}')
				rename(join(root, file), f'{target_file}{suffix}{extension}')
