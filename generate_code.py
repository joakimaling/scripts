#!/usr/bin/python3
"""Generates a QR code for something"""
#                     ___           __  ___
#  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
# / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
# \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
#         https://gitlab.com/carolinealing    /___/
#
# ~/Scripts/generate_code.py
#
# Requires: qrcode, pillow
#

# Common modules
from argparse import Namespace
from dataclasses import dataclass
from os.path import splitext
import sys

# Third-party modules
from qrcode import QRCode # type:ignore
from qrcode.image.pil import PilImage # type:ignore
from qrcode.image.svg import SvgImage # type:ignore

# Utility modules
from tools.parser import CustomArgumentParser


@dataclass
class CodeGenerator:
	"""Creates a QR code from string data.

	Parameters
	----------
	data: str
		Data to be represented as a QR code
	target: str
		Path to where the resulting image will be stored
	"""
	data: str
	target: str = '.'

	def run(self) -> int:
		"""Creates a QR code from string data.

		Returns
		-------
		int
			Return code
		"""
		factory = PilImage if splitext(self.target)[1] == '.png' else SvgImage

		code: QRCode = QRCode(border=2, box_size=10, image_factory=factory)

		code.add_data(self.data)

		code.make(fit=True)

		# Create a black & white image
		image: PilImage|SvgImage = code.make_image(back_color='white', fill='black')

		# Create the file
		image.save(self.target)

		return 0


def main() -> int:
	"""Creates a QR code from string data.

	Returns
	-------
	int
		Return code
	"""
	parser: CustomArgumentParser = CustomArgumentParser(
		description=''
	)

	parser.add_argument(
		'data',
		help='Data to be represented as a QR code'
	)

	parser.add_argument(
		'target',
		default='.',
		help='Path to where the resulting image will be stored'
	)

	arguments: Namespace = parser.parse_args()

	generator: CodeGenerator = CodeGenerator(arguments.data, arguments.target)

	return generator.run()


if __name__ == '__main__':
	sys.exit(main())
