#!/usr/bin/python
"""Counts & sorts frequent entries in a CSV file"""
#                     ___           __  ___
#  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
# / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
# \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
#         https://gitlab.com/carolinealing    /___/
#
# ~/Scripts/list_frequent_items.py
#
# Counts the number of times a certain type of alarm appears on the
# On Call spreadsheet
#

# Standard modules
import csv
import sys

# Stores the number of times a string appears in a list
item_type: dict[str, int] = {}

with open(sys.argv[1], 'r', encoding='utf-8') as file:
	data = csv.reader(file)
	for row in data:
		item: str = row[3].strip().lower().replace(' ', '_')

		# Don't count weird stuff
		if not item or item in ('*', '-'):
			continue

		if item in item_type:
			item_type[item] += 1

		else:
			item_type[item] = 1

# Sort the result on the number of appearances
item_type = dict(sorted(item_type.items(), key=lambda item: item[1]))

# Print it nicely
for key, value in item_type.items():
	print(f'{value:>3} {key}')
