#!/usr/bin/python
"""Get the city & country associated with an IP address"""
#                     ___           __  ___
#  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
# / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
# \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
#         https://gitlab.com/carolinealing    /___/
#
# ~/Scripts/check_host.py
#

# Common modules
import sys

# Third-party modules
from requests.exceptions import JSONDecodeError, RequestException # type:ignore
from requests import get # type:ignore

# Utility modules
from tools.log import Log

# Setting up logs
log = Log(__file__, handlers=('console',)) # pylint: disable=superfluous-parens

# Get hostname
host: str = sys.argv[1]

try:
	with get(f'https://ipapi.co/{host}/json/', timeout=10) as response:
		if response.status_code == 200:
			data: dict = response.json()

			log.info(
				'%s, %s',
				data['city'],
				data['country_code']
			)

except JSONDecodeError as error:
	log.error('JSON Decode error: %s', error)

except RequestException as error:
	log.error('Request error: %s', error)
